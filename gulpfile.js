var gulp = require('gulp'),
    config = require('./gulpconfig')(),
    connect = require('gulp-connect'),
    $ = require('gulp-load-plugins')({lazy: true}),
    gulpIf = require('gulp-if'),
    fileExists = require('file-exists'),
    clean = require('gulp-clean'),
    fileSort = require('gulp-angular-filesort'),
    concat = require('gulp-concat'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    bourbon = require('node-bourbon'),
    notify = require("gulp-notify"),
    plumber = require('gulp-plumber'),
    templateCache = require('gulp-angular-templatecache'),
    babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('clean-main', function () {
    return gulp.src('./app/build/js/main.min.js', {read: false})
        .pipe(gulpIf(fileExists('./app/build/js/main.min.js'), clean()));
});

gulp.task('js', ['clean-main'], function () {
    gulp.src([

        './app/**/*.js',
    ])
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('main.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./app/build/js/'))
        .pipe(connect.reload());
});

gulp.task('css', function () {
    gulp.src([
        './app/css/font-awesome.min.css',
        './node_modules/bootstrap/dist/css/bootstrap.css',
        './node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
        './node_modules/angular-toastr/dist/angular-toastr.min.css',
        './node_modules/angular-confirm1/dist/angular-confirm.min.css',
        './app/css/style.css'
    ])
        .pipe(concat('main.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./app/build/css'))
        .pipe(connect.reload())
});

gulp.task('connect', function () {
    connect.server({
        port: 8091,
        root: config.appDir,
        livereload: true
    });
});

gulp.task('watch', function () {
    gulp.watch('/app/**/*.js', ['js']);
    gulp.watch('/app/**/*.css', ['css']);
});

gulp.task('default', ['connect', 'watch', 'js', 'css']);
