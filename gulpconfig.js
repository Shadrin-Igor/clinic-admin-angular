'use strict';

module.exports = function() {
    return {
        appDir: '.',
        buildDir: './app/build/',
        appIndex: './index.html',

        js: [
            './*.js',
            'app/**/*.js',
            '!app/main/main.min.js'
        ],

        html: [
            './app/*.html',
            './app/main/**/*.html',
            './app/main/**/**/*.html'
        ],

        css: [
            './*.css',
            'app/assets/css/**/*.css'
        ],

        sass: [
            './*.sass',
            'app/sass/**/*.sass'
        ],

        img: [
            './app/assets/img/**/*'
        ],

        templates: [
            './app/templates/**/*'
        ]
    };
};