(()=>{
    'use strict';

    angular.module("mainApp")
        .controller("MainController", ($scope, $rootScope, $log, $state, AuthService)=>{
            $scope.userAuth = AuthService.isAuthenticated();

            $scope.logout = ()=>{
                $rootScope.$broadcast('logout', {});
                AuthService.logout();
                $scope.userAuth = false;
                $state.go('login');
            };

            $scope.$on("login", function (event) {
                $log.info('login');
                $scope.userAuth = true;
            });

        })

})();