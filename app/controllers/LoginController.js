(()=>{
    'use strict';

    angular.module('mainApp')
        .controller('LoginController', function ($scope, $state, $log, AuthService, toastr) {
            $scope.login = ()=> {

                if( $scope.user && $scope.user.login && $scope.user.password ){

                    AuthService.login($scope.user)
                        .then((msg)=> {
                                $state.go('index');
                            },
                            (error)=> {
                                toastr.error(error);
                            });

                } else {
                    toastr.error('Вы не заполнили обязательные поля');
                }

            }
        });

})();