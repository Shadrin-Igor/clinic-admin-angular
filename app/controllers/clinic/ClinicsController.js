(function () {

    angular.module('mainApp')
        .controller('ClinicsController', ($scope, $state, $ngConfirm, toastr, AuthService, RestService) => {

            $scope.req = {};
            RestService.get("/admins/clinics")
                .then((response) => {
                    $scope.items = [];
                    const resItems = response.data.data.slice,
                        listItems = [];
                    if (resItems.length > 0) {

                        for (var i = 0; i < resItems.length; i++) {
                            listItems.push(resItems[i]);
                        }
                        $scope.gridOptions.data = listItems;
                    }

                }, (error) => {
                    console.log('error', error);
                });

            $scope.confirmPopup = (id) => {

                $ngConfirm({
                    title: 'Внимание!',
                    content: 'Вы действительно хотите заблокировать клинику #'+id+ '?',
                    //contentUrl: 'template.html', // if contentUrl is provided, 'content' is ignored.
                    scope: $scope,
                    buttons: {
                        // long hand button definition
                        ok: {
                            text: "заблокировать",
                            btnClass: 'btn-primary',
                            keys: ['enter'], // will trigger when enter is pressed
                            action: (scope) => {
                                $scope.deleteItem(id);
                            }
                        },
                        // short hand button definition
                        'закрыть': (scope) => {
                        }
                    },
                });

            }

            $scope.deleteItem = (id) => {
                if (id) {
                    RestService.delete("/admins/clinics/" + id)
                        .then((item) => {
                            if (item.data.status == "success" && item.data.data.id) {
                                $("#myModal").modal('hide');
                                toastr.success("Клиника успешно деактивирована");
                                //$state.go('clinicProfile.settings', {clinicId: item.data.data.id});
                            }

                        }, (err) => {
                            toastr.error("При деактивировации произошла ошибка("+err+")");
                        })
                }
                else toastr.error('Произошла ошибка');
            }

            $scope.regClinic = (data) => {
                if (data.name && data.email) {
                    RestService.post("/admins/clinics", data)
                        .then((item) => {
                            if (item.data.status == "success" && item.data.data.id) {
                                $(".modal-backdrop").remove();
                                $("#myModal").modal('hide');
                                $state.go('clinicProfile.settings', {clinicId: item.data.data.id});
                            }

                        }, (err) => {
                            let errMessage = '';
                            if (err.data.data["email"] || err.data.data["name"]) errMessage = err.data.data.email || err.data.data.name;
                            if (err.data.data.message) errMessage = err.data.data.message;
                            toastr.error(errMessage);
                        })
                }
                else toastr.error('Все поля являются обязательными');
            }

            $scope.gridOptions = {
                data: [], //required parameter - array with data
                //optional parameter - start sort options
                sort: {
                    predicate: 'companyName',
                    direction: 'asc'
                }
            };

            $scope.login = () => {

            }

        });

})();