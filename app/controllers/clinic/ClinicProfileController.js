(function () {

    angular.module('mainApp')
        .controller('ClinicProfileController', ($scope, $stateParams, AuthService, RestService)=> {
            $scope.data = {};
            if($stateParams.clinicId){
                RestService.get('/admins/clinics/'+$stateParams.clinicId)
                    .then((response)=>{
                        //console.log('response', response);
                        $scope.data = response.data.data;
                    },(err)=>{
                        console.log('err', err);
                    })
            }
        });

})();