(function () {

    angular.module('mainApp')
        .controller('ClinicReceptionsController', ($scope, $stateParams, AuthService, RestService)=> {
            $scope.list = {};
            if($stateParams.clinicId){
                RestService.get('/admins/review/clinics/'+$stateParams.clinicId+'/require/')
                    .then((response)=>{
                        //console.log('response', response);
                        $scope.reviews = response.data.data;
                    },(err)=>{
                        console.log('err', err);
                    })
            }
        });

})();