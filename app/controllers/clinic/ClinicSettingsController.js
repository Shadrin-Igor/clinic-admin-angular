(function () {

    angular.module('mainApp')
        .controller('ClinicSettingsController', ($scope, $stateParams, $http, AuthService, RestService, FileUploader, API_ENDPOINT, toastr) => {

            console.log('data', $scope.$parent.data);
            $scope.tab = 'profile';
            $scope.listSpec = [];
            $scope.listServies = [];
            $scope.password = {};
            $scope.API_ENDPOINT = API_ENDPOINT.url;

            $scope.profile = {};
            $scope.address = $scope.$parent.data.address;
            $scope.profileSpecs = JSON.parse(JSON.stringify($scope.$parent.data.spec));
            $scope.services = $scope.$parent.data.medical_services;

            if ($scope.address && $scope.address.latitude && $scope.address.longitude) {

                if ($scope.address.latitude && $scope.address.longitude)
                    $scope.address.center = [$scope.address.latitude, $scope.address.longitude];


                $scope.address.zoom = 15;
                $scope.address.marker = {
                    geometry: {
                        type: "Point",
                        coordinates: $scope.address.center
                    }
                };
            } else {
                $scope.address = {
                    country: 'Kazahstan',
                    city: {id: 1, name: ''},
                    district: {id: 0, name: ''},
                    street: '',
                    house: '',
                    room: ''
                };
                $scope.address.zoom = 10;
                $scope.address.center = [76.92858, 43.25654];
                $scope.address.marker = {
                    geometry: {
                        type: "Point",
                        coordinates: $scope.address.center
                    }
                };

                $scope.$parent.data.address = $scope.address;
            }

            var map,
                mapClick = false;
            $scope.catalog = {
                cities: [],
                district: []
            };

            loadData();

            $scope.mapClick = function (e) {
                $scope.address.marker.geometry.coordinates = e.get('coords');
                mapClick = true;
                $scope.setAddress($scope.address.marker.geometry.coordinates);
            };

            $scope.afterMapInit = function ($map) {
                map = $map;
                var query = '';
                $scope.yandexMapPreloader = false;
                $scope.$watchGroup([
                    'address.city.id',
                    'address.district.id',
                    'address.street',
                    'address.house'
                ], function () {
                    if (!mapClick) {
                        var query = 'Казахстан ';
                        if ($scope.address) {
                            if ($scope.address.city) {
                                for (var i = 0; i < $scope.catalog.cities.length; i++) {
                                    if ($scope.address.city.id == $scope.catalog.cities[i].value) {
                                        $scope.address.city.name = $scope.catalog.cities[i].title;
                                        query += ' ' + $scope.address.city.name;
                                    }
                                }
                            }
                            if ($scope.address.district) {
                                for (var i = 0; i < $scope.catalog.district.length; i++) {
                                    if ($scope.address.district.id == $scope.catalog.district[i].value) {
                                        $scope.address.district.name = $scope.catalog.district[i].title;
                                        query += ' ' + $scope.address.district.name;
                                    }
                                }
                            }
                            $scope.address.street = $scope.address.street ? $scope.address.street : '';
                            $scope.address.house = $scope.address.house ? $scope.address.house : '';
                            $scope.setAddress(query + ' ' + $scope.address.street + ' ' + $scope.address.house);

                        }
                    }
                    mapClick = false;
                });
            }

            $scope.setAddress = function (coords) {
                ymaps.geocode(coords).then(function (res) {
                    if (typeof coords == 'string') {
                        var bounds = res.geoObjects.get(0).properties.get('boundedBy');
                        map.setBounds(bounds);
                        $scope.address.zoom = map.getZoom();
                        var firstGeoObject = res.geoObjects.get(0);
                    } else {
                        res.geoObjects.each(function (obj) {
                            switch (obj.properties._data.metaDataProperty.GeocoderMetaData.kind) {
                                case 'province':
                                    $scope.address.city.name = obj.properties.get('name');
                                    for (var i = 0; i < $scope.catalog.cities.length; i++) {
                                        if ($scope.address.city.name == $scope.catalog.cities[i].title) {
                                            $scope.address.city.id = $scope.catalog.cities[i].value;
                                        }
                                    }
                                    break;
                                case 'district':
                                    $scope.address.district.name = obj.properties.get('name');
                                    for (var i = 0; i < $scope.catalog.district.length; i++) {
                                        console.log($scope.address.district.name == $scope.catalog.district[i].title);
                                        if ($scope.address.district.name == $scope.catalog.district[i].name) {
                                            $scope.address.district.id = $scope.catalog.district[i].id;
                                        }
                                    }
                                    break;
                                case 'street':
                                    $scope.address.street = obj.properties.get('name');
                                    break;
                                case 'house':
                                    var house = obj.properties.get('name').split(',');
                                    $scope.address.house = house[house.length - 1];
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                    $scope.address.marker.geometry.coordinates = $scope.address.center = firstGeoObject ? firstGeoObject.geometry.getCoordinates() : coords;
                    $scope.$apply();

                });
            }

            for (let i = 0; i < $scope.listSpec.length; i++) {
                for (let n = 0; n < $scope.$parent.data.specs.length; n++) {
                    if ($scope.$parent.data.specs[n].id == $scope.listSpec[i].id) {
                        $scope.specvalue[i] = id;
                    }
                }
            }

            let contacts = $scope.$parent.data.contacts;

            if (contacts) {
                for (let i = 0; i < contacts.length; i++) {
                    if (contacts[i].contact_type == 'email') {
                        $scope.profile.email = contacts[i].contact_element;
                        $scope.profile.email_id = contacts[i].id;
                    }
                    if (contacts[i].contact_type == 'phone') {
                        $scope.profile.tel = contacts[i].contact_element;
                        $scope.profile.tel_id = contacts[i].id;
                    }
                }
            }

            $scope.saveProfile = () => {

                let form = {
                    name: $scope.$parent.data.name,
                    can_be_called: $scope.$parent.data.can_be_called,
                    can_be_called_emergency: $scope.$parent.data.can_be_called_emergency
                }
                // Сохранияем профиль
                RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/profile', form)
                    .then((res) => {
                        if (res.data.status && res.data.status == 'success') {
                            toastr.success("Название успешно обновленно");
                        }
                        else toastr.error("Ошибка сохранения");

                    }, (error) => {
                        toastr.error("Ошибка");
                    });

                // Сохранияем контакты
                let contactData = [];
                console.log('saveProfile', $scope.profile);
                if ($scope.profile.tel) {
                    contactData.push({
                        contact_element: $scope.profile.tel,
                        contact_type: 'phone',
                        contact_element_type: 'work',
                        id: $scope.profile.tel_id
                    });
                }
                if ($scope.profile.email) {
                    contactData.push({
                        contact_element: $scope.profile.email,
                        contact_type: 'email',
                        contact_element_type: 'work',
                        id: $scope.profile.email_id
                    });
                }

                RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/contacts', contactData)
                    .then((res) => {
                        if (res.data.status && res.data.status == 'success') {
                            toastr.success("Контакты успешно сохранены");
                        }
                        else toastr.error("Ошибка сохранения контактов");

                    }, (error) => {
                        toastr.error("Ошибка сохранения контактов");
                    });

            }

            $scope.saveAddress = (form) => {

                let address = {};
                address.country = form.country;
                address.city_id = form.city.id;
                address.district_id = form.district.id;
                address.street = form.street;
                address.house = form.house;
                address.room = form.room;
                address.longitude = form.longitude;
                address.latitude = form.latitude;

                if (address.country || address.city_id || address.district_id) {

                    RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/addresses', address)
                        .then((res) => {
                            if (res.data.status && res.data.status == 'success') {
                                toastr.success("Адресс успешно сохранен");
                            }
                        }, (err) => {
                            toastr.error('Ошибка сохранения адресса')
                        });

                }
                else toastr.error('Незаполнены обязательные поля');


            }

            $scope.saveSpec = (specs) => {

                let inputSpecs = $("input[name=specs]");
                let listSpec = [];
                for (let i = 0; i < inputSpecs.length; i++) {
                    if (inputSpecs[i].checked) listSpec.push(+inputSpecs.eq(i).val());
                }

                RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/specs', listSpec)
                    .then((res) => {
                        if (res.data.status && res.data.status == 'success') {
                            toastr.success("Специализации успешно обновленны");
                        }
                    }, (err) => {
                        toastr.error('Ошибка сохранения специализации')
                    });
            }

            $scope.saveAbout = (about) => {

                RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/about', {about: about})
                    .then((res) => {
                        if (res.data.status && res.data.status == 'success') {
                            toastr.success("Описание о клинике обновленно");
                        }
                    }, (err) => {
                        toastr.error('Ошибка сохранение текста о клинике')
                    });

            }

            $scope.saveService = () => {

                let inputServices = $("input[name=service]");
                let listService = [];
                for (let i = 0; i < inputServices.length; i++) {
                    if (inputServices[i].checked) listService.push(+inputServices.eq(i).val());
                }

                RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/medical-services', listService)
                    .then((res) => {
                        if (res.data.status && res.data.status == 'success') {

                            let form = {
                                name: $scope.$parent.data.name,
                                can_be_called: $scope.$parent.data.can_be_called,
                                can_be_called_emergency: $scope.$parent.data.can_be_called_emergency
                            }
                            // Сохранияем профиль
                            RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/profile', form)
                                .then((res) => {
                                    if (res.data.status && res.data.status == 'success') {

                                        toastr.success("Услуги клиники успешно обновленны");
                                    }
                                    else toastr.error("Ошибка сохранения");

                                }, (error) => {
                                    toastr.error("Ошибка");
                                });

                        }
                    }, (err) => {
                        toastr.error('Ошибка сохранение услуг клиники')
                    });
            };

            $scope.savePassword = (password) => {

                console.log('password', password, $scope.password);
                if (password.old && password.new && password.new2) {
                    if (password.new == password.new2) {

                        RestService.patch('/admins/clinics/' + $scope.$parent.data.id + '/password', {
                            old_password: password.old,
                            new_password: password.new
                        })
                            .then((res) => {
                                if (res.data.status && res.data.status == 'success') {
                                    toastr.success("Пароль успешно обновленн");
                                }
                            }, (err) => {
                                if (err.data.message == 'Access is denied')
                                    toastr.error('Вы ввели не верный старый пароль');
                                else  toastr.error(err)
                            });
                    }
                    else toastr.error('Новый пароль и подтверждение не совпадают');
                }
                else toastr.error('Все поля являются обязательными для заполнения');


            };

            $scope.getChecked = (id) => {

                if ($scope.$parent.data.specs) {
                    for (let i = 0; i < $scope.$parent.data.specs.length; i++) {
                        if ($scope.$parent.data.specs[i] && $scope.$parent.data.specs[i].id == id) {
                            //$scope.specvalue[i] = id;
                            console.log('getChecked', id, '+');
                            return true;
                        }
                    }
                }

                console.log('getChecked', id, '-');
                return false;
            };

            $scope.setChecked = (spec) => {
                for(let i=0;i<$scope.$parent.data.specs.length;i++){
                    if( $scope.$parent.data.specs[i].id == spec.id ){
                        $scope.$parent.data.specs.splice(i, 1);
                        return;
                    }

                }
                console.log('spec', spec.id);
                $scope.$parent.data.specs.push(spec);
            }

            $scope.getCheckedServies = (id) => {
                if ($scope.$parent.data.medical_services) {
                    for (let i = 0; i < $scope.$parent.data.medical_services.length; i++) {
                        if ($scope.$parent.data.medical_services[i].id == id)return true;
                    }
                }

                return false;
            };

            $scope.uploaderLogo = new FileUploader({
                url: API_ENDPOINT.url + '/files/upload/',
                //formData: {type:'galleries'},
                headers: {
                    'X-Authorization': $http.defaults.headers.common['X-Authorization'],
                    'Accept-Language': 'ru'
                },
                removeAfterUpload: true,
                autoUpload: true
            });

            $scope.uploaderLogo.formData.push({type: 'avatars'});

            $scope.uploaderLogo.onSuccessItem = function (fileItem, response, status, headers) {

                console.log(response);
                if (response.status == 'success') {

                    let form = {
                        name: $scope.$parent.data.name,
                        can_be_called: $scope.$parent.data.can_be_called,
                        can_be_called_emergency: $scope.$parent.data.can_be_called_emergency,
                        upload_id: response.data
                    }
                    //console.log(response.data.data);
                    // Сохранияем профиль
                    RestService.put('/admins/clinics/' + $scope.$parent.data.id + '/profile', form)
                        .then((res) => {
                            if (res.data.status && res.data.status == 'success') {
                                $scope.$parent.data.profile_photo_url = {id: response.data[0]};
                                toastr.success("Аватар успешно обновленн");
                            }
                            else toastr.error("Ошибка сохранения");

                        }, (error) => {
                            toastr.error("Ошибка");
                        });
                }


            };
            $scope.uploaderLogo.onErrorItem = function (fileItem, response, status, headers) {
                console.log('onErrorItem', fileItem, response, status, headers);
            };

            $scope.uploader = new FileUploader({
                url: API_ENDPOINT.url + '/files/upload/',
                //formData: {type:'galleries'},
                headers: {
                    'X-Authorization': $http.defaults.headers.common['X-Authorization'],
                    'Accept-Language': 'ru'
                },
                removeAfterUpload: true,
                autoUpload: true
            });

            $scope.uploader.formData.push({type: 'galleries'});

            $scope.imageDelete = (id) => {
                RestService.delete('/admins/clinics/' + $scope.$parent.data.id + '/gallery', {ids: [id]})
                    .then((res) => {
                        if (res.data.status && res.data.status == 'success') {

                            RestService.get('/admins/clinics/' + $scope.$parent.data.id + '/gallery')
                                .then((res) => {
                                    if (res.data.status && res.data.status == 'success') {
                                        $scope.$parent.data.gallery_urls = res.data.data;
                                        toastr.success("Картинка успешно обновленна");
                                    }
                                }, (err) => {
                                    toastr.error("Произошла ошибка закачки файла");
                                    console.log('uploader image', err);
                                });

                        }
                    }, (err) => {
                        toastr.error("Произошла ошибка закачки файла");
                        console.log('uploader image', err);
                    });

            }

            $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.data) {

                    RestService.post('/admins/clinics/' + $scope.$parent.data.id + '/gallery', {ids: response.data})
                        .then((res) => {
                            if (res.data.status && res.data.status == 'success') {

                                RestService.get('/admins/clinics/' + $scope.$parent.data.id + '/gallery')
                                    .then((res) => {
                                        if (res.data.status && res.data.status == 'success') {
                                            $scope.$parent.data.gallery_urls = res.data.data;
                                            toastr.success("Галлерея успешно обновленна");
                                        }
                                    }, (err) => {
                                        toastr.error("Произошла ошибка закачки файла");
                                        console.log('uploader image', err);
                                    });

                            }
                        }, (err) => {
                            toastr.error("Произошла ошибка закачки файла");
                            console.log('uploader image', err);
                        });
                }
            };
            $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
                console.log('onErrorItem', fileItem, response, status, headers);
            };

            function loadData() {

                RestService.get('/admins/catalogs/cities?sortBy=')
                    .then((res) => {
                        if (res.data.data.slice) {

                            $scope.catalog.cities = res.data.data.slice;
                        }
                    }, (err) => {
                        toastr.error(err)
                    });

                if ($scope.address && $scope.address.city && $scope.address.city.id) {

                    RestService.get('/catalogs/cities/' + $scope.address.city.id + '/districts')
                        .then((res) => {
                            if (res.data.data) {

                                $scope.catalog.district = res.data.data;
                            }
                        }, (err) => {
                            alert(err)
                        });
                }

                RestService.get('/admins/catalogs/specs?sortBy=')
                    .then((res) => {
                        if (res.data.data.slice) {
                            $scope.listSpec = res.data.data.slice;
                        }
                    }, (err) => {
                        toastr.error(err)
                    });

                RestService.get('/admins/catalogs/specs/medical-services?sortBy=')
                    .then((res) => {
                        if (res.data.data.slice) {
                            $scope.listServies = res.data.data.slice;
                        }
                    }, (err) => {
                        toastr.error(err)
                    });
            }
        });

})
();