(function () {

    angular.module('mainApp')
        .controller('ClinicReviewsController', ($scope, $stateParams, AuthService, RestService)=> {
            $scope.reviews = {};
            if($stateParams.clinicId){
                RestService.get('/clinics/'+$stateParams.clinicId+'/reviews')
                    .then((response)=>{
                        //console.log('response', response);
                        $scope.reviews = response.data.data;
                    },(err)=>{
                        console.log('err', err);
                    })
            }
        });

})();