(function () {

    angular.module('mainApp')
        .controller('IndexController', ($scope, AuthService)=> {

            $scope.login = ()=> {
                AuthService.login($scope.user)
                    .then((msg)=> {
                            $state.go('index');
                        },
                        (error)=> {
                            $log.error(error);
                        });
            }

        });

})();