(()=> {

    angular.module('mainApp')

        .service('AuthService', function ($q, $rootScope, $http, API_ENDPOINT) {
            var LOCAL_TOKEN_KEY = 'token';
            var authenticated = false;
            var authToken;

            function loadUserCredentials() {
                var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
                if (token) {
                    useCredentials(token);
                    console.log("loadUserCredentials");
                }
            }

            function storeUserCredentials(token) {
                window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
                useCredentials(token);
            }

            function useCredentials(token) {
                authenticated = true;
                authToken = token;
                console.log('authenticated', authenticated);
                $rootScope.$broadcast('login', {});

                $http.defaults.headers.common["X-Authorization"] = 'Bearer ' + authToken;
                $http.defaults.headers.common["Content-Type"] = 'application/json';
                $http.defaults.headers.common["Accept-Language"] = 'ru';
            }

            function destroyUserCredentials() {
                authToken = undefined;
                authenticated = false;
                $http.defaults.headers.common["X-Authorization"] = undefined;
                window.localStorage.removeItem(LOCAL_TOKEN_KEY);
            }

            var register = function (user) {
                return $q(function (resolve, reject) {
                    $http.post(API_ENDPOINT.url + '/signup', user).then(function (result) {
                        if (result.data.success) {
                            resolve(result.data.msg);
                        } else {
                            reject(result.data.msg);
                        }
                    });
                });
            };

            var login = function (user) {
                if (user && user.login && user.password) {

                    return $q(function (resolve, reject) {
                        $http.post(API_ENDPOINT.url + '/admins/profile/login', {
                            user_name: user.login,
                            password: user.password
                        }).then(function (result) {

                            if (result.status == '200' && result.data.data.access_token) {

                                storeUserCredentials(result.data.data.access_token);
                                resolve(result.status);
                            } else {
                                reject("error");
                            }
                        }, function (error) {

                            console.log(error);
                            reject(error.data.message);

                        });
                    });

                }

            };

            var logout = function () {
                destroyUserCredentials();
            };

            loadUserCredentials();

            return {
                login: login,
                register: register,
                logout: logout,
                isAuthenticated: function () {
                    return authenticated;
                },
            };
        })

        .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
            return {
                responseError: function (response) {
                    $rootScope.$broadcast({
                        401: AUTH_EVENTS.notAuthenticated,
                    }[response.status], response);
                    return $q.reject(response);
                }
            };
        })

        .config(function ($httpProvider) {
            $httpProvider.interceptors.push('AuthInterceptor');
        });

})();