(function(){

    angular.module('mainApp')
        .service('RestService', function($q, $http, API_ENDPOINT) {

            function send(method, url, data){

                $http.defaults.headers.common['Content-Type'] = 'application/json';
                $http.defaults.headers.common['Accept-Language'] = 'ru';
                $http.defaults.headers.common['Accept'] = 'application/json;charset=UTF-8';

                return $http({
                    method: method,
                    url: API_ENDPOINT.url+url,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept-Language': 'ru'
                    },
                    data: data || ''
                });
            }

            function get(url, data) {
                return send('GET', url, data);
            }

            function put(url, data) {
                return send('PUT', url, data);
            }

            function post(url, data) {
                return send('POST', url, data);
            }

            function patch(url, data) {
                return send('patch', url, data);
            }

            function del(url, data) {
                return send('DELETE', url, data);
            }

            return {
                post: post,
                put: put,
                patch: patch,
                get: get,
                delete: del
            };
        })


})();