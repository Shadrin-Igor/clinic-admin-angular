(function(){

    angular.module('mainApp')
        .constant('AUTH_EVENTS', {
            notAuthenticated: 'auth-not-authenticated'
        })
        .constant('API_ENDPOINT', {
            //url: 'https://ucare.uz:9090/api/v1'
            url: 'https://kliniki.spg.uz:8443/api/v1'
        })

})();