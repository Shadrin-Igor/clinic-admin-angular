angular.module( 'mainApp', ['ui.router', 'dataGrid', 'cp.ngConfirm', 'pagination', 'angularFileUpload', 'toastr', 'yaMap'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'app/views/index.html',
                controller: 'IndexController'
            })
            .state('login', {
                url : '/login',
                templateUrl: 'app/views/login.html',
                noLogin: true,
                controller: 'LoginController',
            })
            .state('receptions', {
                url : '/receptions',
                templateUrl: 'app/views/receptions.html',
                controller: 'ReceptionsController',
            })
            .state('clinics', {
                url : '/clinics',
                templateUrl: 'app/views/clinic/clinics.html',
                controller: 'ClinicsController',
            })
            .state('clinicProfile', {
                abstract: true,
                controller: 'ClinicProfileController',
                templateUrl: 'app/views/clinic/clinicProfile.html',
                params : {
                    clinicId: null
                }
            })
            .state('clinicProfile.info', {
                url : '/clinic-profile',
                templateUrl: 'app/views/clinic/clinicProfileInfo.html',
                controller: 'ClinicInfoController',
            })
            .state('clinicProfile.settings', {
                url : '/clinic-profile',
                templateUrl: 'app/views/clinic/clinicProfileSettings.html',
                controller: 'ClinicSettingsController',
                params : {
                    clinicId: null
                }
            })
            .state('clinicProfile.reviews', {
                url : '/clinic-reviews',
                templateUrl: 'app/views/clinic/clinicProfileReviews.html',
                controller: 'ClinicReviewsController',
            })
            .state('clinicProfile.receptions', {
                url : '/clinic-receptions',
                templateUrl: 'app/views/clinic/clinicProfileReceptions.html',
                controller: 'ClinicReceptionsController',
            })
            .state('doctors', {
                url : '/doctors',
                templateUrl: 'app/views/doctors.html',
                controller: 'DoctorsController',
            })
            .state('labs', {
                url : '/labs',
                templateUrl: 'app/views/labs.html',
                controller: 'LabsController',
            })
            .state('patients', {
                url : '/patients',
                templateUrl: 'app/views/patients.html',
                controller: 'PatientsController',
            })
            .state('reviews', {
                url : '/reviews',
                templateUrl: 'app/views/reviews.html',
                controller: 'ReviewsController',
            })
            .state('complaints', {
                url : '/complaints',
                templateUrl: 'app/views/complaints.html',
                controller: 'ComplaintsController',
            })
            .state('general', {
                url : '/general',
                templateUrl: 'app/views/general.html',
                controller: 'GeneralController',
            })
            .state('gatalogs', {
                url : '/gatalogs',
                templateUrl: 'app/views/gatalogs.html',
                controller: 'GatalogsController',
            });

        $urlRouterProvider.otherwise('/');

    })

    .run(function ($rootScope, $state, $http, $log, AuthService) {

        
        $rootScope.$on('$stateChangeStart', function (event,next, nextParams, fromState) {
            if (!AuthService.isAuthenticated()) {
                if (next.name !== 'login' && next.name !== 'register') {
                    event.preventDefault();
                    $state.go('login');
                }
            }

            $rootScope.activeTab = next.name;
        });

        $rootScope.$on('$stateChangeError',function (event, toState, toParams, fromState, fromParams, error) {
                if (!angular.isString(error)) {
                    error = JSON.stringify(error);
                }
                $log.error('$stateChangeError: ' + error);
            }
        );

    });